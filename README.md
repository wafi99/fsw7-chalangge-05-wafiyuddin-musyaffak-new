# Car Management Dashboard
description ...\
Proses Belum Fix All

## How to run
guide ...

```bash
how to run
-- yarn start
-- localhost:9090 (untuk masuk ke form)
--localhost:9090/index_cars.html (masuk ke dashboard)
```

## Endpoints
list endpoints ...

## Directory Structure

```
.
├── config
│   └── config.json
├── controllers
├── migrations
├── models
│   └── index.js
├── public
│   ├── css
│   ├── fonts
│   ├── img
│   ├── js
│   └── uploads
├── seeders
├── views
│   ├── templates
│   │   ├── footer.ejs
│   │   └── header.ejs
│   ├── form.ejs
│   └── list.ejs
├── .gitignore
├── README.md
├── index.js
├── package.json
└── yarn.lock
```

## ERD
![Entity Relationship Diagram](public/img/erd_fix.png)
